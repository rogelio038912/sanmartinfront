import { Routes } from '@angular/router';
import { ProspectosComponent } from './prospectos/prospectos.component';

export const routes: Routes = [
    
  { path: '', component: ProspectosComponent }
];


import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiUtilsService } from '../api-utils.service';
import { Prospecto } from '../Models/prospecto';
import { FormsModule } from '@angular/forms';
import { Archivo } from '../Models/archivo';
import { environment } from '../../environments/environment.development';
import { estatus } from '../Models/estatus';

@Component({
  selector: 'app-prospectos',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './prospectos.component.html',
  styleUrl: './prospectos.component.css'
})

export class ProspectosComponent implements OnInit {
  constructor(private ApiUtilsService: ApiUtilsService) { }
  public listaPrincipal: Prospecto[] = [];
  public prospecto: Prospecto = new Prospecto();
  public estatus: estatus[] = [];
  public infoEstatus: estatus = new estatus();
  public infoProspecto: Prospecto = new Prospecto();
  public fileUpload: any;
  public nombre: string = '1';
  public mostrarComentario: boolean = false;
  public infoVacia: boolean = false;
  public estatusSelect: string = '';
  public comentarioEstatus: string = '';
  public url: string = environment.urlApi

  ngOnInit(): void {
    console.log("AQUI MERO")
    let data: any = {
      nombre: '',
      info: null
    }
    this.prospecto.archivos.push(data)
    this.obtenerLista()
    this.obtenerEstatus()
  }
  openUrl(url: string) {
    window.open(url, '_blank');
  }
  asignarEstatus(item: estatus) {
    this.infoEstatus = item
    this.infoEstatus.comentario = this.comentarioEstatus
  }
  guardarEstatus(item: estatus) {

    console.log("this.estatusSelect")
    console.log(this.estatusSelect)
    if (item.nombre == "Rechazado" && this.comentarioEstatus == '') {
      this.mostrarComentario = true
    } else {
      let data = {
        id: this.infoProspecto.id,
        idEstatus: item.id,
        comentario: this.comentarioEstatus
      }
      this.ApiUtilsService.post('Prospectos/GuardaEstatus', data).then((res: any) => {
        this.comentarioEstatus = ''
        this.mostrarComentario = false
        document.getElementById("closeM")?.click()
        this.obtenerLista()
        console.log("this.infoProspecto")
        console.log(this.infoProspecto)
      })

    }
  }
  obtenerLista() {
    this.ApiUtilsService.get('Prospectos/ObtenerProspectos').then((res: any) => {
      this.listaPrincipal = res;
      console.log("this.listaPrincipal")
      console.log(this.listaPrincipal)
    })
  }
  obtenerEstatus() {
    this.ApiUtilsService.get('Prospectos/estatusProspectos').then((res: any) => {
      this.estatus = res;
      console.log("this.estatus")
      console.log(this.estatus)
    })
  }

  obtenerProspecto(id: any) {
    let data = {
      id: id
    }
    this.ApiUtilsService.post('Prospectos/InfoProspecto', data).then((res: any) => {
      this.infoProspecto = res;
      console.log("this.infoProspecto")
      console.log(this.infoProspecto)
    })
  }
  guardar() {
    console.log("this.infoProspecto")
    console.log("entro guardar")
    if (
      this.prospecto.nombre == '' ||
      this.prospecto.apellidoPat == '' ||
      this.prospecto.calle == '' ||
      this.prospecto.numero == '' ||
      this.prospecto.colonia == '' ||
      this.prospecto.codigoPostal == '' ||
      this.prospecto.telefono == '' ||
      this.prospecto.rfc == '' 
    ) {
      console.log("entro")
      console.log(this.prospecto)
      this.infoVacia=true
    } else {
      console.log("salio")
      var info = new FormData();
      console.log(this.prospecto);
      info.append("nombre", this.prospecto.nombre);
      info.append("apellidoPat", this.prospecto.apellidoPat);
      info.append("apellidoMat", this.prospecto.apellidoMat);
      info.append("calle", this.prospecto.calle);
      info.append("numero", this.prospecto.numero);
      info.append("colonia", this.prospecto.colonia);
      info.append("telefono", this.prospecto.telefono);
      info.append("codigoPostal", this.prospecto.codigoPostal);
      info.append("rfc", this.prospecto.rfc);
      let num = 0
      for (let i = 0; i < this.prospecto.archivos.length; i++) {
        if (this.prospecto.archivos[i].info != null) {
          num++;
          info.append(`file${num}`, this.prospecto.archivos[i].info);
          info.append(`nombreA${num}`, this.prospecto.archivos[i].nombre);
        }
      }
      info.append("numArchivos", String(num));

      console.log(info.has('nombre')); // Debería devolver true si 'id' está presente en el FormData
      console.log(info.get('nombre'));


      this.ApiUtilsService.postFile('Prospectos/Guardar', info).then((res: any) => {
        document.getElementById("close")?.click()
        this.infoVacia=false
        this.obtenerLista()
        //window.location.reload();
        console.log(res)
      })

    }
  }

  agregarDocumento() {

    let data: any = {
      nombre: '',
      info: null
    }
    this.prospecto.archivos.push(data)
    console.log(this.prospecto.archivos)
  }
  vaciarProspecto() {
    this.prospecto = new Prospecto();
    let data: any = {
      nombre: '',
      info: null
    }
    this.infoVacia=false
    this.prospecto.archivos.push(data)
  }
  upload(event: any, item: any) {
    item.info = event.target.files[0];
  }

}

import { Archivo } from "./archivo";

export class Prospecto
 {
    id              :string='';               
    nombre          :string='';    
    apellidoPat     :string='';        
    apellidoMat     :string='';        
    calle           :string='';    
    numero          :string='';    
    colonia         :string='';    
    codigoPostal    :string='';            
    telefono        :string='';        
    rfc             :string='';
    estatus         :string='';
    archivos      :Archivo[]=[];
  }
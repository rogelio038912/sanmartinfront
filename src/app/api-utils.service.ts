import { Injectable } from '@angular/core';
import { environment } from '../environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class ApiUtilsService {

  constructor() {}
  public urlApi: string = environment.urlApi
  
   public get(Url:string){

    var myInit = { 
        method: 'Get'
    };
    return new Promise((resolve, reject) => {
      fetch(environment.urlApi+Url,myInit).then((response)=>{
        console.log(response.status)
        if (response.status==200){
          response.json().then((data)=>{
            resolve(data);
          })
        }else{
          reject(response)
        }
      }).catch((e) => {
        reject(e);
      })
    })
    
   }
  public async postFile(Url:string,data: any) {
    var myInit = { 
        method: 'Post',
        body: data
    };
    return new Promise((resolve, reject) => {
      fetch(environment.urlApi+Url,myInit).then((response)=>{
        console.log(response.status)
        if (response.status==200){
          response.json().then((data)=>{
            resolve(data);
          })
        }else{
          reject(response)
        }

      }).catch((e) => {
        reject(e);
      })
    })
  }
  public async post(Url:string,json: any) {
    var dataset=JSON.stringify(json);
    console.log(dataset)
    var myInit = { 
        method: 'Post',
        headers: {
          "Content-Type": "application/json"
        },
        body: dataset
    };
    return new Promise((resolve, reject) => {
      fetch(environment.urlApi+Url,myInit).then((response)=>{
        console.log(response.status)
        if (response.status==200){
          response.json().then((data)=>{
            resolve(data);
          })
        }else{
          reject(response)
        }
      }).catch((e) => {
        reject(e);
      })
    })
  }
}
